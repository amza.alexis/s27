const http = require('http');

const PORT = 4000;

const server = http.createServer((request, response) => {

        //booking system
    if (request.url === '/') {

        response.writeHead(200, { 'Content-Type': 'text/plain' });
        response.end('Welcome to Booking System!');

        //profile
    } else if (request.url === '/profile' && request.method === 'GET') {

        response.writeHead(200, { 'Content-Type': 'text/plain' })
        response.end('Welcome to your profile!')

        //courses
    } else if (request.url === '/courses' && request.method === 'GET') {

        response.writeHead(200, { 'Content-Type': 'text/plain' })
        response.end(`Here's our courses available`)

        //addcourse
    } else if (request.url === '/addcourse' && request.method === 'POST') {

        response.writeHead(200, { 'Content-Type': 'text/plain' })
        response.end('Add a course to our resources')

        //updatecourse
    } else if (request.url === '/updatecourse' && request.method === 'PUT') {

        response.writeHead(200, { 'Content-Type': 'text/plain' })
        response.end('Update a course to our resources')

        //archivecourse
    } else if (request.url === '/archivecourses' && request.method === 'DELETE') {

        response.writeHead(200, { 'Content-Type': 'text/plain' })
        response.end('Archive courses to our resources')

    } else {
        response.writeHead(404, { 'Content-Type': 'text/plain' });
        response.end('Error 404: Page not found.');
    }
})

server.listen(PORT);

console.log(`Server is running at localhost:${PORT}`);
