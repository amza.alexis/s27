/*
	HTTP Methods
		HTTP methods of the incoming request can be accessed via the "method" property of the request parameter.

		GET - method for a request that indicates that we want to retrieve data.

		POST - method for a request that indicates that we want to add or send data to the server for processing. It is usually used for creating new entries or documents.

		PUT - method for a request that indicates that we want to edit or update

		DELETE - method to delete a specified document 
*/

const http = require('http');

const PORT = 4000;

let directory = [
	
	{
		"name": "John",
		"email": "john@mail.com",
	},
	{
		"name": "Jane",
		"email": "jane@mail.com",
	}
]

http.createServer(function (request, response){

	if (request.url === "/"){

		console.log(request.method)
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('New Server')
	} else if (request.url === "/users" && request.method === "GET"){
		response.writeHead(200, {"Content-Type": "json/application"})
		response.end(JSON.stringify(directory))
	} else if (request.url === "/users" && request.method === "POST") {
		let requestBody = ' ';

		request.on('data', function(data){

			requestBody += data;
		})

		request.on('end', function(){
			requestBody = JSON.parse(requestBody);
			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			}

			directory.push(newUser);
			console.log(directory);

			response.writeHead(200, {'Content-Type': 'application/json'})
			response.end(JSON.stringify(newUser))
		})
	} else if (request.url === "/users" && request.method === "DELETE") {
		directory.pop();
		response.writeHead(200, {'Content-Type': 'plain/text'})
		response.end("Item Deleted")
		console.log(directory);
	} else if (request.url === "/users" && request.method === "PUT"){

		let requestBody = '';

		request.on('data', function(data){
			requestBody += data;
		})

		request.on('end', function(){
			requestBody = JSON.parse(requestBody);

			directory[requestBody.index] = {
				name: requestBody.name,
				email: requestBody.email
			}

			console.log(directory[requestBody.index]);

			response.writeHead(200, {'Content-Type': 'application/json'})
			response.end(JSON.stringify(directory[requestBody.index]))
		})
	}
}).listen(PORT)

console.log(`Server is running at localhost: ${PORT}.`)
